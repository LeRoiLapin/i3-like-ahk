moveTo(dir) {
	WinGetTitle, Title, A
	if (dir = 0) {
		; Left
		WinMove, %Title%, , -11, 0, (A_ScreenWidth / 2), (A_ScreenHeight)
	} else if (dir = 1) {
		; Up
		WinMove, %Title%, , 0, 0, (A_ScreenWidth), (A_ScreenHeight / 2)
	} else if (dir = 2) {
		; Right
		WinMove, %Title%, , (A_ScreenWidth / 2), 0, (A_ScreenWidth / 2), (A_ScreenHeight)
	} else if (dir = 3) {
		; Down
		WinMove, %Title%, , 0, (A_ScreenHeight / 2), (A_ScreenWidth), (A_ScreenHeight / 2)
	}
}

#+Left::moveTo(0)
#+Up::moveTo(1)
#+Right::moveTo(2)
#+Down::moveTo(3)